# docker-privoxy

Privoxy is a "privacy enhancing proxy", filtering Web pages and removing advertisements. This docker container runs privoxy over Alpine Linux in 6 Mb uncompressed image.

The image, that is built every week, can be pulled from [caligari/privoxy](https://hub.docker.com/r/caligari/privoxy)

Source repository: https://gitlab.com/caligari/docker-privoxy

## Run privoxy

(please, __read the security note below__)

    docker run -d --name=privoxy -p 8118:8118 caligari/privoxy:latest
    
## Test privoxy

    curl http://ifconfig.es
    curl --proxy http://localhost:8118/ http://ifconfig.es
      
## Security note

This container should be executed in a local host or in a VPN.

Please, don't execute this docker in a host accesible from the internet with -p parameter because it __bypass the Linux firewall__ and you could be opening an __anonymous internet door__. Bind the port to the interface of your VPN:

    VPN_IFACE_IP=10.131.1.1 docker run -d -p ${VPN_IFACE_IP}:8118:8118 caligari/privoxy:latest

## Compose

Docker compose example:

```yaml
version: "3"

services:
  app:
    image: "caligari/privoxy:latest"
    container_name: "privoxy"
    restart: "unless-stopped"
    ports:
      - "10.131.1.1:8118:8118" # bind to your VPN interface IP
```

